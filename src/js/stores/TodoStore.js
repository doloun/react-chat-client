const AppDispatcher = require('../dispatchers/AppDispatcher');
const Constants = require('../constants/AppConstants');
const BaseStore = require('./BaseStore');
const assign = require('object-assign');

// data storage
let _data = [];
var stompClient = null;

if (stompClient == null) {
  connect()
}

function connect() {
  var socket = new SockJS('http://localhost:8080/hello');
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function(frame) {
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/greetings', function(greeting){
      _data.push({title:JSON.parse(greeting.body).content})
      console.log(JSON.parse(greeting.body).content);
      TodoStore.emitChange();
    });
  });
}


// add private functions to modify data
function addItem(title, completed=false, aaa="bbb") {
  //_data.push({title, completed, aaa});
  stompClient.send("/app/hello", {}, JSON.stringify({ 'name': title }));
}

// Facebook style store creation.
function clearTasks() {
  _data.length = 0;
}
let TodoStore = assign({}, BaseStore, {

  // public methods used by Controller-View to operate on data
  getAll() {
    return {
      messages: _data
    };
  },

  // register store with dispatcher, allowing actions to flow through
  dispatcherIndex: AppDispatcher.register(function(payload) {
    let action = payload.action;

    switch(action.type) {
      case Constants.ActionTypes.ADD_TASK:
        let text = action.text.trim();
        // NOTE: if this action needs to wait on another store:
        // AppDispatcher.waitFor([OtherStore.dispatchToken]);
        // For details, see: http://facebook.github.io/react/blog/2014/07/30/flux-actions-and-the-dispatcher.html#why-we-need-a-dispatcher
        if (text !== '') {
          addItem(text);
          TodoStore.emitChange();
        }
        break;
      case Constants.ActionTypes.CLEAR_TASK:
            clearTasks();
        TodoStore.emitChange();
            break;
      // add more cases for other actionTypes...
    }
  })

});

module.exports = TodoStore;
