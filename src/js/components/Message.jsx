const React = require('react');
const ActionCreator = require('../actions/TodoActionCreators');
const ListGroupItem = require('react-bootstrap/lib/ListGroupItem');
const Input = require('react-bootstrap/lib/Input');
const Inspector = require('react-json-inspector');

let Message = React.createClass({
  getDefaultProps() {
    return {
      message: {
        title: '',
        completed: false
      }
    };
  },

  //handleToggle(task) {
  //  if (this.refs.checkbox.getDOMNode().checked) {
  //    ActionCreator.completeTask(task);
  //  }
  //},

  render() {
    let {message} = this.props;
    return (
      <ListGroupItem>
        {/*
        <Inspector data={ message } />
        */}
        {message.title}
      </ListGroupItem>
    );
  }
});

module.exports = Message;
