const React = require('react');
const TodoStore = require('../stores/TodoStore');
const ActionCreator = require('../actions/TodoActionCreators');
const Button = require('react-bootstrap/lib/Button');
const Jumbotron = require('react-bootstrap/lib/Jumbotron');
const MessageList = require('./MessageList.jsx');

let App = React.createClass({

  getInitialState() {
    return {
      messages: []
    }
  },

  _onChange() {
    this.setState(TodoStore.getAll());
  },

  componentDidMount() {
    TodoStore.addChangeListener(this._onChange);
  },

  componentWillUnmount() {
    TodoStore.removeChangeListener(this._onChange);
  },

  handleAddNewClick(e) {
    let title = prompt('Enter message');
    if (title) {
      ActionCreator.addItem(title);
    }
  },

  handleClearListClick(e) {
    ActionCreator.clearList();
  },

  render() {
    let {messages} = this.state;
    return (
      <div className="container">
        <Jumbotron>
          <h1><i className="fa fa-comments-o"></i> React Chat Client</h1>
        </Jumbotron>

        <MessageList messages={messages} />

        <Button onClick={this.handleAddNewClick} bsStyle="primary">Send message</Button>
        <Button onClick={this.handleClearListClick} bsStyle="danger">Clear List</Button>
      </div>
    );
  }

});

module.exports = App;
