const React = require('react');
const Message = require('./Message.jsx');
const ListGroup = require('react-bootstrap/lib/ListGroup');
const Alert = require('react-bootstrap/lib/Alert');

let MessageList = React.createClass({
  getDefaultProps() {
    return {
      messages: []
    };
  },

  render() {
    let {messages} = this.props;

    if (messages.length === 0) {
      return (
        <Alert bsStyle="warning">
          No meessages
        </Alert>
      );
    }

    return (
      <form>
        <ListGroup>
          {messages.map(message =>
            <Message message={message} />
          )}
        </ListGroup>
      </form>
    );
  }
});

module.exports = MessageList;
